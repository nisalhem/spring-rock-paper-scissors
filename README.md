# spring-rock-paper-scissors

## Setup overview
- Database: MongoDB<br />
- IDE: Idea IntelliJ Ultimate<br />
- tournament-service port:8080<br />
- bot-player-service port:8081<br />
- match-room-service port:8082<br />

## Player related
### POST requests

1. Register a player (create a player database profile): 127.0.0.1:8080/api/player<br />
Body:{"name": "{playerName}"}

### GET requests

1. Display all the registered players: 127.0.0.1:8080/api/player/get

2. Display the count of registered players: 127.0.0.1:8080/api/player/getcount

3. Remove player from database: 127.0.0.1:8080/api/player/{playerID}/remove<br />
(Please expect a status:500.) 
- Firstly this is due to a fault from my side by adding; @RequestBody instead of @PathVariable in the function removePlayer(@PathVariable String playerId) in PlayerController. But these two annotations have been used in other places in the code. This is just one point that I randomly missed out, but only saw
after committing to the git. By replacing @RequestBody with @PathVariable, the functionality should work.
- After replacing @RequestBody with @PathVariable, status:500 could also be raised, if the playerID is already removed from the database. IDE should then throw a "Player ID not found" exception.

4. Create a bot player: 127.0.0.1:8080/api/player/getbot

## Tournament related
### POST requests

1. Register a tournament(create a Tournament database profile): 127.0.0.1:8080/api/tournament<br />
Body:{"name": "{tournamentName}"}

2. Add participants to the tournament: 127.0.0.1:8080/api/tournament/{tournamentID}/players<br />
Body:{"id":"{playerID}"}

3. Start tournament: 127.0.0.1:8080/api/tournament/{tournamentID}/start<br />
(Please expect a status:500. To start the tournament, match rooms should be created. The error comes when creating a match-room in match-room-service.)

### GET requests

1. Display all registered tournaments: 127.0.0.1:8080/api/tournament/get

2. Display winner of the tournament: 127.0.0.1:8080/api/tournament/{tournamentID}/winner

## Bot-player related

### POST requests

1. Create bot player: 
- via port 8080 (PlayerController): 127.0.0.1:8080/api/bot
- via port 8081 (BotPlayerController): 127.0.0.1:8081/api/bot

## Match-room related
1. Create match room: 127.0.0.1:8082/api/room<br />
(Staus:500 should be expected. This is the cause for the status:500 that was explained in the request
 'Start tournament'.)

<img src="Images/design_rok_paper_scissors.png">




