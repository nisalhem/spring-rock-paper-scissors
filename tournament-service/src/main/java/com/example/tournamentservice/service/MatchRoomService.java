package com.example.tournamentservice.service;

import com.example.tournamentservice.model.BotPlayer;
import com.example.tournamentservice.model.MatchRoom;
import com.example.tournamentservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MatchRoomService {
    @Autowired
    RestTemplate restTemplate;
    @Value("${service.matchroom}")
    String matchRoomServiceUrl;

    public MatchRoom createMatchRoom(){
        return restTemplate.postForEntity(matchRoomServiceUrl + Constants.CREATE_MATCH_ROOM,null, MatchRoom.class).getBody();
    }
}
