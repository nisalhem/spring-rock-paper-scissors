package com.example.tournamentservice.service;

import com.example.tournamentservice.dto.AddPlayerDTO;
import com.example.tournamentservice.exception.NotFoundException;
import com.example.tournamentservice.model.Player;
import com.example.tournamentservice.model.Tournament;
import com.example.tournamentservice.repository.TournamentRepository;
import com.example.tournamentservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;

@Service
public class TournamentService {

    @Autowired
    TournamentRepository tournamentRepository;
    @Autowired
    BotPlayerService botPlayerService;
    @Autowired
    MatchRoomService matchRoomService;

    public Tournament createTournament(Tournament tournament){
        tournament.setStarted(false);
        return tournamentRepository.save(tournament);
    }

    public List<Tournament> get(){
        return tournamentRepository.findAll();
    }

    public Tournament startTournament(String tournamentId){
        Optional<Tournament> tournament = tournamentRepository.findById(tournamentId);
        if (tournament.isPresent()){
            Tournament tempTournament = tournament.get();
            tempTournament = matchParticipants(tempTournament);
            tempTournament = assignToRooms(tempTournament);
            tempTournament.setStarted(true);
            return tempTournament;
        } else {
            throw new NotFoundException(Constants.TOURNAMENT_NOT_FOUND_EXCEPTION);
        }
    }

    private Tournament assignToRooms(Tournament tournament) {
        int totalParticipantCount = tournament.getParticipantsCount() + tournament.getBotPlayersCount();
        int roomsCount = (int)totalParticipantCount/2;
        for(int i=0; i<roomsCount; i++){
            tournament.addToMatchRoomDraw(i, matchRoomService.createMatchRoom());
        }
        return tournament;
    }

    public Tournament addParticipants(String tournamentId, AddPlayerDTO player){
        Optional<Tournament> tournament = tournamentRepository.findById(tournamentId);
        if (tournament.isPresent()){
            Tournament tempTournament = tournament.get();
            tempTournament.addParticipants(player.getPlayerId());
            return tournamentRepository.save(tempTournament);
        } else {
            throw new NotFoundException(Constants.TOURNAMENT_NOT_FOUND_EXCEPTION);
        }
    }

    private Tournament matchParticipants(Tournament tournament){
        if(tournament.getParticipants() != null){
            int participantsCount = tournament.getParticipants().size();
            int M;
            if(participantsCount == 1){
                M = 1;
            }else{
                M = (int)Math.sqrt(participantsCount) + 1;
            }

            int botsCount = (int)Math.pow(2, M) - participantsCount;
            if(tournament.getBotPlayersCount() > botsCount){
                while (tournament.getBotPlayersCount() > botsCount){
                    tournament.removeBotParticipants();
                }
            }else {
                while (tournament.getBotPlayersCount() < botsCount){
                    tournament.addBotParticipants(botPlayerService.createBotPlayer());
                }
            }
        }
        return tournament;
    }

    public String getWinner(String tounamentId){
        return tournamentRepository.findById(tounamentId).get().getWinner();
    }
}
