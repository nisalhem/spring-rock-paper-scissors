package com.example.tournamentservice.service;

import com.example.tournamentservice.exception.NotFoundException;
import com.example.tournamentservice.model.Player;
import com.example.tournamentservice.repository.PlayerRepository;
import com.example.tournamentservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    public Player createPlayer(Player player){
        return playerRepository.save(player);
    }

    public List<Player> get(){
        return playerRepository.findAll();
    }

    public long getCount(){
        return playerRepository.count();
    }

    public void removePlayer(String playerId){
        if(playerRepository.existsById(playerId)){
            playerRepository.deleteById(playerId);
        }else {
            throw new NotFoundException(Constants.PLAYER_ID_NOT_FOUND_EXCEPTION);
        }
    }
}
