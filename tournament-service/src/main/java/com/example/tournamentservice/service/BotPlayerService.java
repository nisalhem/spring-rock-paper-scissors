package com.example.tournamentservice.service;

import com.example.tournamentservice.model.BotPlayer;
import com.example.tournamentservice.model.Player;
import com.example.tournamentservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class BotPlayerService {
    @Autowired
    RestTemplate restTemplate;
    @Value("${service.botplayer}")
    String botPlayerServiceUrl;

    public BotPlayer createBotPlayer(){
        return restTemplate.postForEntity(botPlayerServiceUrl + Constants.CREATE_BOT_PLAYER,null, BotPlayer.class).getBody();
    }
}
