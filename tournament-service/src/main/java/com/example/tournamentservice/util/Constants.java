package com.example.tournamentservice.util;

public class Constants {
    public static String CREATE_BOT_PLAYER = "/api/bot";
    public static String CREATE_MATCH_ROOM = "/api/room";
    public static String TOURNAMENT_NOT_FOUND_EXCEPTION = "Tournament not found";
    public static String PLAYER_ID_NOT_FOUND_EXCEPTION = "Player ID not found";

}
