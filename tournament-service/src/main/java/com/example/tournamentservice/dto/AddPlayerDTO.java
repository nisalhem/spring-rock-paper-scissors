package com.example.tournamentservice.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AddPlayerDTO {
    String playerId;

    @JsonCreator
    public AddPlayerDTO(@JsonProperty("id") String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerId() {
        return playerId;
    }
}
