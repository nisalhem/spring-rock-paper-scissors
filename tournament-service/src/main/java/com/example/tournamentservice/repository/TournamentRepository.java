package com.example.tournamentservice.repository;

import com.example.tournamentservice.model.Tournament;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TournamentRepository extends MongoRepository<Tournament,String> {
}
