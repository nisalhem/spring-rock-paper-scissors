package com.example.tournamentservice.model;

public class BotPlayer {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
