package com.example.tournamentservice.model;

import com.example.tournamentservice.exception.NotFoundException;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

@Document
public class Tournament {
    @Id
    String id;
    String name;
    String winner;
    List<String> participants;
    List<BotPlayer> botParticipants;
    HashMap<Integer, MatchRoom> matchRoomDraw;
    boolean started;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }

    public void addParticipants(String playerId){
        if(this.participants == null){
            this.participants = new ArrayList<>();
            this.participants.add(playerId);
        }else if(!this.participants.contains(playerId)){
            this.participants.add(playerId);
        }
    }

    public int getParticipantsCount(){
        if(this.participants != null){
            return this.participants.size();
        } else{
            return 0;
        }
    }

    public List<BotPlayer> getBotParticipants() {
        return botParticipants;
    }

    public void setBotParticipants(List<BotPlayer> botParticipants) {
        this.botParticipants = botParticipants;
    }

    public void addBotParticipants(BotPlayer botParticipant) {
        if(this.botParticipants == null){
            this.botParticipants = new ArrayList<>();
        }
        this.botParticipants.add(botParticipant);
    }

    public void removeBotParticipants(){
        if(this.participants != null && this.participants.size() > 0){
            this.participants.remove(0);
        }
    }

    public int getBotPlayersCount(){
        if(this.botParticipants != null){
            return this.botParticipants.size();
        } else{
            return 0;
        }
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public void addToMatchRoomDraw(int matchNumber, MatchRoom matchRoom){
        if(this.matchRoomDraw == null){
            this.matchRoomDraw = new HashMap<Integer, MatchRoom>();
        }
        this.matchRoomDraw.put(Integer.valueOf(matchNumber), matchRoom);
    }
}
