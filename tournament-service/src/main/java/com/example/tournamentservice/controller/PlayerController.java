package com.example.tournamentservice.controller;

import com.example.tournamentservice.model.BotPlayer;
import com.example.tournamentservice.model.Player;
import com.example.tournamentservice.service.BotPlayerService;
import com.example.tournamentservice.service.PlayerService;
import com.example.tournamentservice.service.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/player")
public class PlayerController {

    @Autowired
    PlayerService playerService;
    @Autowired
    BotPlayerService botPlayerService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Player createPlayer(@RequestBody Player player) {
        return playerService.createPlayer(player);
    }

    @GetMapping("/getbot")
    @ResponseStatus(HttpStatus.OK)
    public BotPlayer getBotPlayer(){
        return botPlayerService.createBotPlayer();
    }

    @GetMapping("/get")
    @ResponseStatus(HttpStatus.OK)
    public List<Player> get(){
        return playerService.get();
    }

    @GetMapping("/getcount")
    @ResponseStatus(HttpStatus.OK)
    public long getCount(){
        return playerService.getCount();
    }

    @GetMapping("/{playerId}/remove")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removePlayer(@RequestBody String playerId){
        playerService.removePlayer(playerId);
    }
}
