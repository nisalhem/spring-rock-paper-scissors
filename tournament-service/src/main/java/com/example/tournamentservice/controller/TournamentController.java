package com.example.tournamentservice.controller;

import com.example.tournamentservice.dto.AddPlayerDTO;
import com.example.tournamentservice.model.Player;
import com.example.tournamentservice.model.Tournament;
import com.example.tournamentservice.service.TournamentService;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tournament")
public class TournamentController {
    @Autowired
    TournamentService tournamentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Tournament createTournament(@RequestBody Tournament tournament) {
        return tournamentService.createTournament(tournament);
    }

    @GetMapping("/get")
    @ResponseStatus(HttpStatus.OK)
    public List<Tournament> get(){
        return tournamentService.get();
    }

    @PostMapping(value = "/{tournamentId}/start")
    @ResponseStatus(HttpStatus.OK)
    public Tournament startTournament(@PathVariable String tournamentId) {
        return tournamentService.startTournament(tournamentId);
    }

    @PostMapping(value = "/{tournamentId}/players")
    @ResponseStatus(HttpStatus.OK)
    public Tournament addParticipants(@PathVariable String tournamentId, @RequestBody AddPlayerDTO player) {
        return tournamentService.addParticipants(tournamentId, player);
    }

    @GetMapping("/{tournamentId}/winner")
    @ResponseStatus(HttpStatus.OK)
    public String getWinner(@PathVariable String tournamentId){
        return tournamentService.getWinner(tournamentId);
    }
}
