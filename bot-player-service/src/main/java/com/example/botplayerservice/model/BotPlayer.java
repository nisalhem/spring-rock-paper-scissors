package com.example.botplayerservice.model;

public class BotPlayer {
    String name;

    public BotPlayer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPlayingChoice(){
        return (int) ((Math.random() * 2));
    }
}
