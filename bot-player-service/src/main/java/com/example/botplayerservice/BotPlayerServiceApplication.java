package com.example.botplayerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotPlayerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(BotPlayerServiceApplication.class, args);
    }

}
