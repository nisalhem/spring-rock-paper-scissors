package com.example.botplayerservice.controller;

import com.example.botplayerservice.model.BotPlayer;
import com.example.botplayerservice.service.BotPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/bot")
public class BotPlayerController {

    @Autowired
    BotPlayerService botPlayerService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BotPlayer createBotPayer() {
        return botPlayerService.createBotPlayer();
    }
}
