package com.example.botplayerservice.service;

import com.example.botplayerservice.model.BotPlayer;
import org.springframework.stereotype.Service;

@Service
public class BotPlayerService {

    public BotPlayer createBotPlayer(){
        return new BotPlayer(generateBotName());
    }

    private String generateBotName(){
        return "Bota";
    }
}
