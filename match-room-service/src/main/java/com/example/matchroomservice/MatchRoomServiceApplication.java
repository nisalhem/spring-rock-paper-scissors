package com.example.matchroomservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchRoomServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MatchRoomServiceApplication.class, args);
    }

}
