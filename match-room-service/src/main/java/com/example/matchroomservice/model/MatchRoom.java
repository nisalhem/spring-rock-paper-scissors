package com.example.matchroomservice.model;

public class MatchRoom {
    int matchRoomNumber;

    public MatchRoom(int matchRoomNumber) {
        this.matchRoomNumber = matchRoomNumber;
    }

    public int getMatchRoomNumber() {
        return matchRoomNumber;
    }

    public void setMatchRoomNumber(int matchRoomNumber) {
        this.matchRoomNumber = matchRoomNumber;
    }
}
