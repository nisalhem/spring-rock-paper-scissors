package com.example.matchroomservice.controller;

import com.example.matchroomservice.model.MatchRoom;
import com.example.matchroomservice.service.MatchRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/room")
public class MatchRoomController {
    @Autowired
    MatchRoomService matchRoomService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MatchRoom createMatchRoom(){
        return matchRoomService.createMatchRoom();
    }
}
