package com.example.matchroomservice.service;

import com.example.matchroomservice.model.MatchRoom;
import org.springframework.stereotype.Service;

@Service
public class MatchRoomService {

    public MatchRoom createMatchRoom(){
        return new MatchRoom(1);
    }
}
